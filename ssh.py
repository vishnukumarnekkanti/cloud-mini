#!/usr/bin/python
import os
from getpass import getpass

import paramiko

def deploy_key(key, server, username, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, username=username, password=password)
    client.exec_command('mkdir -p ~/.ssh/')
    client.exec_command('echo "%s" > ~/.ssh/authorized_keys' % key)
    client.exec_command('chmod 644 ~/.ssh/authorized_keys')
    client.exec_command('chmod 700 ~/.ssh/')
    client.exec_command('mkdir -p ~/.vishnu_imgs/')

'''
key = open(os.path.expanduser('~/.ssh/id_rsa.pub')).read()
#username = os.getlogin()
#password = getpass()
hosts = ["10.1.33.81"] #, "hostname2", "hostname3"]
for host in hosts:
    username = raw_input("user name for " + host + " : ").strip()
    password = getpass()
    deploy_key(key, host, username, password)
'''

def setSSH(hosts, imgs_list):
    key = open(os.path.expanduser('~/.ssh/id_rsa.pub')).read()
    pm_list = []
    for host in hosts:
        username = raw_input("user name for " + host + " : ").strip()
        password = getpass()
        pm_list.append(username+'@'+host)
        deploy_key(key, host, username, password)
        for x in imgs_list:
            os.system("scp " + x + " " + username + "@" + host + ":~/.vishnu_imgs/" )
    with open('vishnu_pms.json', 'w') as fp:
        fp.write(",".join(pm_list))

if __name__ == '__main__':
    hosts = ["10.1.33.81","10.1.34.132","10.1.36.68"] #, "hostname2", "hostname3"]
    setSSH(hosts)
