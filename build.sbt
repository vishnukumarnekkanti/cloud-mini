name := """miniproject-1"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator


fork in run := true

libraryDependencies += "org.libvirt" % "libvirt" % "0.5.1"
//libraryDependencies += "com.sun.jna" % "jna" % "3.0.9"
libraryDependencies += "net.java.dev.jna" % "jna" % "4.1.0"
libraryDependencies += "com.google.code.gson" % "gson" % "2.3.1"
libraryDependencies += "commons-io" % "commons-io" % "2.4"
