package controllers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.*;
//import play.api.libs.json.Json;
import play.libs.Json;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

	protected Virt v = new Virt();
	
    public Result index() {
        try{
        	System.out.println("entered");
        v.testConnection();
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
        return ok(index.render("Your new application is ready."));
    }
    
    public Result createVM(String name, int instance_type, int image_id){
        if (name == null || name.isEmpty())  
            return badRequest("Invalid Request");
        System.out.println(name + " " + instance_type + " " + image_id);
        int vmid = v.newVm(name, instance_type, image_id);
        ObjectNode result = Json.newObject();
        result.put("vmid",Integer.toString(vmid));
        return ok(result);
    }
    
    static String readFile(String path, Charset encoding) 
  		  throws IOException 
  		{
  		  byte[] encoded = Files.readAllBytes(Paths.get(path));
  		  return new String(encoded, encoding);
  		}
    
    public Result queryVM(int vmid){
    	VM vm = v.VMQuery(vmid);
    	ObjectNode result = Json.newObject();
    	if(vm == null){
    		result.put("status","Failed");
    		return ok(result);
    	}
    	result.put("vmid", vmid);
    	result.put("name", vm.name);
    	result.put("pmid", vm.pmid);
    	result.put("instance_type", vm.flavour);
    	return ok(result);
    }
    
    public Result destroyVM(int vmid){
    	ObjectNode result = Json.newObject();
    	if(v.kill(vmid)){
    		result.put("status","1");
    	}else{
    		result.put("status","0");
    	}
    	return ok(result);
    }
    
    public Result types(){
    	List<String> flav;
    	try {
			flav = Arrays.asList(readFile("vishnu_flavours.json", Charset.defaultCharset()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ok("none available");
		}
		ObjectNode result = Json.newObject();
		String f = flav.toString();
		f = f.replaceAll("'", "\"");
    	JsonNode jn = Json.parse(f);
    	result.set("types", jn);
    	return ok(result);
    }
    
    public Result pmlist(){
    	List<Integer> plist = v.ListPM();
    	//Gson gson = new Gson();
    	ObjectNode result = Json.newObject();
    	JsonNode jn = Json.parse(plist.toString());
    	result.set("pmids", jn);
    	return ok(result);
    }
    
    public Result listvms(int pmid){
    	List<Integer> plist = v.VMonPM(pmid);
    	//Gson gson = new Gson();
    	ObjectNode result = Json.newObject();
    	JsonNode jn = Json.parse(plist.toString());
    	result.set("vmids", jn);
    	return ok(result);
    }
    
    
    public Result imgs(){
    	List<String> imglist = v.getImgs();
    	int i =0;
    	List <ObjectNode> ilist = new ArrayList<ObjectNode>();
    	for(String s : imglist){
    		System.out.println("iso link " + s);
    		Path p = Paths.get(s);
    		String file = p.getFileName().toString();
    		file = FilenameUtils.removeExtension(file);
    		ObjectNode im = Json.newObject();
    		im.put("id", i);
    		im.put("name", file);
    		ilist.add(im);
    		i = i + 1;
    	}
    	ObjectNode result = Json.newObject();
    	JsonNode jn = Json.parse(ilist.toString());
    	result.set("images", jn);
    	return ok(result);
    }
    
    public Result queryPM(int pmid){
    	return ok(v.pmQuery(pmid));
    }
    
    public Result createVol(String name, int size){
    	return ok("create vol");
    }
    
    public Result queryVol(int volId){
    	return ok("query vol");
    }
    
    public Result destroyVol(int volId){
    	return ok("destroy vol");
    }
    
    public Result attachVol(int vmId, int volId){
    	return ok("attach vol");
    }
    
    public Result detachVol(int volId){
    	return ok("detatch vol");
    }

}
