package controllers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.libvirt.*;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.google.gson.JsonParser;

import play.libs.Json;

public class Virt{
	Gson gson = new Gson();
	protected List <PM> pmList = new ArrayList<PM>();
	List <String> imgList;
	protected List<VM> vmList = new ArrayList<VM>();
	protected List<Flavour> flavourList = new ArrayList<Flavour>();
	
	public Virt(){
		loadPms();
		loadImgs();
		loadFlavours();
		loadVms();
		System.out.println(System.getProperty("user.home"));
		//loadStoragePools();
	}
	
	public int genRandom() {
	    Random r = new Random( System.currentTimeMillis() );
	    return (1 + r.nextInt(2)) * 1000 + r.nextInt(1000);
	}
	
	public void loadStoragePools(){
		// TODO Auto-generated method stub
		String storagepoolXMLDesc = null;
		try {
			storagepoolXMLDesc = String.format(
					readFile("storagepool_config.xml", Charset.defaultCharset()),
					System.getProperty("user.home") + "/.vishnu_imgs");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				for(PM pm: pmList){
					Connect c = pm.connection;
					try {
						if(c.listDefinedStoragePools().length == 0){
							SPool spool = new SPool();
							spool.sp = c.storagePoolCreateXML(storagepoolXMLDesc, 0);
							System.out.println(c.listDefinedStoragePools());
						}
						else{
							//pm.pool = c.listDefinedStoragePools()[0];
							System.out.println(c.listDefinedStoragePools()  
									+ " " + c.listDefinedStoragePools().length);
						}
					} catch (LibvirtException e) {
						System.out.println("libvirtexception yaar");
						e.printStackTrace();
					}
				}
	}
	
	public void loadVms() {
		// TODO Auto-generated method stub
		//List <PM> tmp = new ArrayList<PM>();
		//for(PM pm: pmList){
		for(int i =0;i<pmList.size();i++){
			Connect c = pmList.get(i).connection;
			try {
				for(int d : c.listDomains()){
					String s = c.domainLookupByID(d).getName();
					VM vm = new VM();
					vm.pmid = pmList.get(i).pmid;
					vm.vmid = (int) (Math.round(Math.random() * 1000) + genRandom());
					vm.flavour = getFlavour(c.domainLookupByName(s));
					vm.UUID = c.domainLookupByName(s).getUUIDString();
					vm.name = s;
					pmList.get(i).freecpu -= c.domainLookupByName(s).getMaxVcpus();
					System.out.println(c.domainLookupByName(s).getMaxVcpus() + " here");
					pmList.get(i).freememory -= c.domainLookupByName(s).getMaxMemory();
					//System.out.println(vm.vmid);
					vmList.add(vm);
					//tmp.add(pm);
					//System.out.println(pm.freecpu + " modified freecpu");
				}
			} catch (LibvirtException e) {
				System.out.println("libvirtexception yaar");
				e.printStackTrace();
			}
		}
		for(PM p : pmList){
			System.out.println(p.freecpu +" = freecpu " + p.ip + " ip");
		}
	}

	private int getFlavour(Domain dom) {
		// TODO Auto-generated method stub
		int id=-1;
		try {
			System.out.println( "domain name : " + dom.getName() + " " + dom.getInfo().memory + ": mem , cpu = "+ dom.getInfo().nrVirtCpu);
		} catch (LibvirtException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(Flavour f: flavourList){
			try {
				if ( dom.getInfo().maxMem/1000 == f.ram && dom.getInfo().nrVirtCpu == f.cpu){
					id = f.typeid;
				}
			} catch (LibvirtException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return id;
	}

	public void loadFlavours() {
		// TODO Auto-generated method stub
		try {
			List<String> flav = Arrays.asList(readFile("vishnu_flavours.json", Charset.defaultCharset()));
			List<Flavour> fList = new Flavour().FlavourList(flav.toString());
			int i=0;
			for(Flavour f: fList){
				f.typeid = i;
				i = i +1;
				//System.out.println(f.typeid + " " + f.cpu);
				flavourList.add(f);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadPms(){
		String pms;
		List <String> plist = null;
		try {
			pms = readFile("vishnu_pms.json", Charset.defaultCharset());
			plist = new ArrayList<String>(Arrays.asList(pms.split(",")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int i = 0;
		for(String s : plist){
			PM pm = new PM();
			pm.pmid = i;
			pm.username = (s.split("@"))[0];
			pm.ip = (s.split("@"))[1];
			//System.out.println(pm.username);
			try {
				Connect c = new Connect("qemu+ssh://" + pm.username + "@" + pm.ip + "/system",false);
				pm.connection = c;
				NodeInfo n = c.nodeInfo();
				pm.cpu = n.maxCpus();
				pm.freecpu = n.maxCpus();
				pm.memory = n.memory;
				pm.freememory = n.memory;
				pmList.add(pm);
			} catch (LibvirtException e) {
				// TODO Auto-generated catch block
				System.out.println("Failed to register physical machine : " + pm.username);
				//e.printStackTrace();
			}
			i=i+1;
		}
	}
	
	public void loadImgs(){
		String imgs;
		try {
			imgs = readFile("vishnu_imgs.json", Charset.defaultCharset());
			imgList = new ArrayList<String>(Arrays.asList(imgs.split(",")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    private static void displayDomainDetails(Connect conn, Domain testDomain)

            throws LibvirtException {

        System.out.println("Domain:" + testDomain.getName() + " id "

         + testDomain.getID() +" max memory "
         +testDomain.getMaxMemory() + " memory :" + testDomain.getInfo().memory);

        

        System.out.println("Domain state: " + testDomain.getInfo().state.name());

    }
    
    static String readFile(String path, Charset encoding) 
    		  throws IOException 
    		{
    		  byte[] encoded = Files.readAllBytes(Paths.get(path));
    		  return new String(encoded, encoding);
    		}
    
    public Integer makeVM(Connect conn, String name, Flavour config, PM pm, int imgIndex ){
    	try {
    		String uuid = UUID.randomUUID().toString();
    		Path p = Paths.get(imgList.get(imgIndex));
    		String file = p.getFileName().toString();
    		//System.out.println("/home/"+ pm.username + "/.vishnu_imgs/" + file);
    		String imfile = "/home/"+ pm.username + "/.vishnu_imgs/" + file;
    		//System.out.println("reached "+ config.ram + " " + config.cpu);
			String xmlDesc = String.format(readFile("vm_config.xml", Charset.defaultCharset()), 
					name, uuid, config.ram*1000, config.ram*1000, config.cpu, imfile);
			//System.out.println(xmlDesc);
			Domain tmpDomain = conn.domainDefineXML(xmlDesc);
			tmpDomain.create();
			displayDomainDetails(conn, tmpDomain);
			return updateVMList(pm.pmid, name, config, uuid);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		} catch (LibvirtException e){
			System.out.println("LibvirtException");
			return 0;
		}
    }
    
    public void testConnection()  throws LibvirtException {
        System.out.println("getConnection invoked");
        Path p = Paths.get(imgList.get(0));
		String file = p.getFileName().toString();
		System.out.println(System.getProperty("user.home") + "/.vishnu_imgs/" + file);
        for(Flavour f : flavourList){
        	System.out.println("id = " + f.typeid + " cpu = " + f.cpu + " memory = " + f.ram);
        }
    }
    
    /*public Boolean newVM(String name, int type){
    	for(PM pm: pmList){
    		Connect c = pm.connection;
    		try {
				long freeMemory = c.getFreeMemory();
				
			} catch (LibvirtException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		return true;
    }*/
    
    public List<Integer> ListPM(){
    	List<Integer> lp = new ArrayList<Integer>();
    	for(PM pm:pmList){
    		lp.add(pm.pmid);
    	}
    	return lp;
    }
    
    public List<Integer> VMonPM(int pmid){
    	List<Integer> lp = new ArrayList<Integer>();
    	for(VM vm:vmList){
    		if(vm.pmid == pmid)
    			lp.add(vm.vmid);
    	}
    	return lp;
    }
    
    public VM VMQuery(int vmid){
        for (VM v: vmList){
        	if(v.vmid == vmid)
        		return v;
        }
    	return null;
    }
    
    public boolean kill(int vmid){
    	System.out.println("killing");
    	for(VM v:vmList){
    		System.out.println("hit kill vm with vmid "+ vmid + " now on " + v.vmid);
    		if(v.vmid == vmid){
    			System.out.println("hit kill vm");
    			for(PM p:pmList){
    				if(v.pmid == p.pmid){
    					try {
    						Domain d = p.connection.domainLookupByUUIDString(v.UUID);
    						System.out.println(d.isActive() + " " + d.isPersistent() );
    						if(d.isActive() == 1){
    							d.shutdown();
    							d.destroy();
    						}
							p.connection.domainLookupByUUIDString(v.UUID).undefine();
							vmList.remove(v);
							loadPms();
							return true;
						} catch (LibvirtException e) {
							e.printStackTrace();
							return false;
						}
    				}
    			}
    		}
    	}
		return false;
    }

	public Integer newVm(String name, int type, int imgId){
		Flavour config = new Flavour();
		for(Flavour f:flavourList){
			if( f.typeid == type){
				config = f;
				break;
			}
		}
		for(VM vm:vmList){
			if(vm.name.equals(name)){
				return -1;
			}
		}
		
		for(PM pm:pmList){
			Connect conn = pm.connection;
			System.out.println(pm.freememory + " " + config.ram);
			if(pm.freecpu >= config.cpu && pm.freememory >= config.ram ){
				return makeVM(conn, name, config, pm, imgId);
			}
		}
		return 2;
	}

	public List<String> getImgs() {
		// TODO Auto-generated method stub
		return imgList;
	}
	
	public Integer updateVMList(int pmid, String name, Flavour config, String uuid){
		VM vm = new VM();
		vm.pmid = pmid;
		vm.vmid = (int) (Math.round(Math.random() * 1000) + genRandom());
		vm.flavour = config.typeid;
		vm.UUID = uuid;
		vm.name = name;
		//System.out.println(vm.vmid);
		vmList.add(vm);
		for(int i = 0 ; i< pmList.size();i++){
			if(pmList.get(i).pmid == pmid){
				pmList.get(i).freecpu -= config.cpu;
				pmList.get(i).freememory -= config.ram*1000;
			}
		}
		return vm.vmid;
	}
	
	public ObjectNode pmQuery(int pmid){
		ObjectNode result = Json.newObject();
		ObjectNode capacity = Json.newObject();
		ObjectNode free = Json.newObject();
		for(PM pm:pmList){
			if(pm.pmid == pmid){
				capacity.put("cpu", pm.cpu);
				capacity.put("ram", pm.memory);
				capacity.put("disk", pm.disk);
				free.put("cpu", pm.freecpu);
				free.put("ram", pm.freememory);
				free.put("disk", pm.disk);
				result.put("pmid",pmid);
				result.set("capacity", capacity);
				result.set("free", free);
				result.put("vms", VMonPM(pmid).size());
			}
			else{
				result.put("status","0");
			}
		}
		return result;
	}
}

