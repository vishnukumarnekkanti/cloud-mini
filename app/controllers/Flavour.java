package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Flavour {
	public int typeid;
	public int cpu;
	public long ram;
	public int disk;
	
	public Flavour(){
		
	}
	
	public List<Flavour> FlavourList(String json){
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        List<Flavour> fl = new ArrayList<Flavour>();
        fl = Arrays.asList(gson.fromJson(json, Flavour[].class));
        return fl;
    }
}
